This repository creates a Docker image based on
[manylinux](https://github.com/pypa/manylinux) that contains the build
dependencies for LALSuite.

## Example .gitlab-ci.yml

```yaml
wheel-cp36-cp36m:
  image: containers.ligo.org/lscsoft/lalsuite-manylinux
  script:
    - opt/python/wheel-cp36-cp36m/bin/python setup.py bdist_wheel
    - auditwheel repair dist/*.whl
  artifacts:
    paths:
      - wheelhouse
```
