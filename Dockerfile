ARG base
FROM $base

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]
SHELL ["/docker-entrypoint.sh", "/bin/sh", "-c"]

# Install dependencies available through yum.
RUN mkdir -p /docker-entrypoint.d && \
    yum install -y \
        fftw3-devel \
        libxml2-devel \
        mpich-devel \
    && yum clean all \
    && echo 'source /usr/share/Modules/init/sh' > /docker-entrypoint.d/00module \
    && echo 'module load mpi' > /docker-entrypoint.d/01mpi

# Configure, make, and make install these packages from source
COPY build.bash /
RUN /build.bash a49e4e09a2e5f6e0ae823a68f02b3f6285cf3e0af66c9a1f8dadcd2867fa83c17fd27e446e8e5af9374ff2dc7b7a18334663ce977a1691c4ed4ced14d33232b7 https://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio-4.4.1.tar.gz
RUN /build.bash 20eec83a3e71a578e9f7615e631ec712774cde3e4e41a4117e26a148dbee611de5133c751ff0c5c9c6b47f66fedd8de4b3cd23146c1006edc38e32913eae7cd1 https://sourceforge.net/projects/healpix/files/Healpix_3.30/chealpix-3.30.0.tar.gz
RUN /build.bash 667a40703b4e330db32d3ede48247781019414426d2949aabf2036089155bc7f2167fbc8b8c78b76286d8369d1f3c9bdd66049d3e4eda2c234ef751df9cc597a https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.4/src/hdf5-1.14.4-3.tar.gz
RUN /build.bash 0b842d7149f8e2b04142135fa99ec0cc5af0361192df8ac8983853f4bd1ce0f65b36d4c396c59605b02bcc69eb15fe2259bfb92c49f82a2bbeb114eed500dfc3 https://git.ligo.org/virgo/virgoapp/Fr/-/archive/8.48.4/Fr-8.48.4.tar.bz2
RUN /build.bash f7c5d7f8862e088c0b7b7050f95af5e8c65234988f8cd337c32a2c2ad7b40030b881b0ae768a66c7f8e0646e1b6a2f64714a66bee6527514af6be60f824b038b https://software.ligo.org/lscsoft/source/metaio-8.5.1.tar.gz
RUN /build.bash 4427f6ce59dc14eabd6d31ef1fcac1849b4d7357faf48873aef642464ddf21cc9b500d516f08b410f02a2daa9a6ff30220f3995584b0a6ae2f73c522d1abb66b https://ftpmirror.gnu.org/gsl/gsl-2.8.tar.gz
RUN rm -f /build.bash

# Install older version of patchelf, because 0.16.1 and newer causes many
# lal binaries (like lalapps_version) to segfault on x86_64.
# FIXME: see https://git.ligo.org/lscsoft/lalsuite/-/issues/626,
# https://github.com/NixOS/patchelf/issues/558.
RUN pipx install --force 'patchelf<0.16.1'
