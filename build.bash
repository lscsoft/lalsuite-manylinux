#!/bin/bash

# Echo commands, and halt on error
set -ex

# Unpack command line arguments
hash=$1
url=$2
shift 2
makeargs="$*"

# Download and verify source
curl -Ls -o src $url
echo "$hash  src" | sha512sum --check

# Build and install
mkdir build
pushd build
tar --transform 's,^\(\./\)*,,' --strip-components 1 -xf ../src
if [ -e configure ]; then
    ./configure --prefix=/usr --libdir=/usr/lib64
else
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr .
fi
    make -j4 $makeargs
    make install
popd

# Cleanup
rm -rf build src
