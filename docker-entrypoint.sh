#!/bin/sh
DIR=/docker-entrypoint.d
if [[ -d ${DIR} ]]; then
    for SCRIPT in $(find ${DIR} -type f | sort); do
        source ${SCRIPT}
    done
fi
if [[ $# -eq 0 ]]; then
    exec "/bin/bash"
else
    exec "$@"
fi
